﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MarsRovers
{
    /// <summary> Represents a Mars Rover. Handles its location and movement. </summary>
    public class Rover
    {
        #region Constants

        /// <summary> The number of possible headings. </summary>
        private static readonly int NUM_DIRECTIONS = Enum.GetValues(typeof(Direction)).Length;

        /// <summary> The amount to increment the integer value of the heading by in order to turn right. </summary>
        private static readonly int RIGHT_INCREMENT = 1;

        /// <summary> The amount to increment the integer value of the heading by in order to turn left. See remarks for details. </summary>
        /// <remarks>
        /// Note that decrementing does not work as intended; negative numbers do not behave as desired with the % operator.
        /// For example:
        ///     Actual:     -1 % 4 == -1
        ///     Expected:   -1 % 4 == 3
        /// 
        /// To get the expected behavior, we can take advantage of the following equality:
        ///     (x + (n - 1)) % n == (x - 1) mod n
        ///     for x greater than -n.
        /// </remarks>
        private static readonly int LEFT_INCREMENT = NUM_DIRECTIONS - 1;

        #endregion

        #region Member Variables and Properties

        private Queue<Command> _commandQueue;

        /// <summary> Gets or sets the simulation this rover belongs to. </summary>
        public Simulation Simulation { get; set; }

        /// <summary> Gets or sets the current location of the rover. </summary>
        public Point Position { get; private set; }

        /// <summary> Gets the direction the rover is heading. </summary>
        public Direction Heading { get; private set; }

        /// <summary> Gets whether the rover has any commands to execute. </summary>
        public bool HasCommands => _commandQueue.Count > 0;

        #endregion

        /// <summary> Creates a new Rover and intializes its position and heading. </summary>
        /// <param name="x">         The initial horizontal coordinate. </param>
        /// <param name="y">         The initial vertical coordinate. </param>
        /// <param name="direction"> The initial heading. </param>
        public Rover(int x, int y, Direction direction)
        {
            Simulation = null;
            Position = new Point(x, y);
            Heading = direction;
            _commandQueue = new Queue<Command>();
        }

        /// <summary> Turns the rover 90 degrees to the left, updating its heading. </summary>
        public void TurnLeft()
        {
            // This approach to turning works as long as all members of the Direction enum point along the XY plane and are ordered properly.
            // If not ordered or not along the XY plane, we'd probably need to use a switch block instead.
            // See the remarks for LEFT_INCREMENT for details on why we're incrementing instead of decrementing.
            int newHeading = ((int)Heading + LEFT_INCREMENT) % NUM_DIRECTIONS;
            Heading = (Direction)newHeading;
        }

        /// <summary> Turns the rover 90 degrees to the right, updating its heading. </summary>
        public void TurnRight()
        {
            // This approach to turning works as long as all members of the Direction enum point along the XY plane and are ordered properly.
            // If not ordered or not along the XY plane, we'd probably need to use a switch block instead.
            int newHeading = ((int)Heading + RIGHT_INCREMENT) % NUM_DIRECTIONS;
            Heading = (Direction)newHeading;
        }

        /// <summary> Moves the rover one unit in the direction it's heading. </summary>
        /// <exception cref="InvalidOperationException"> Thrown if the rover does not have its simulation assigned. </exception>
        public void Move()
        {
            if (Simulation == null)
                throw new InvalidOperationException("No simulation is assigned to the rover.");

            // Determine where the rover will move to.
            Point pt = GetNextPosition();

            // Check for collisions and falls.
            if (pt.X < 0 || pt.Y < 0 || pt.X > Simulation.MaxX || pt.Y > Simulation.MaxY)
                throw new InvalidOperationException($"A rover fell off the {Heading} edge of the plateau at ({Position.X}, {Position.Y}).");
            if (Simulation.Rovers.Any(rover => pt.Equals(rover.Position)))
                throw new InvalidOperationException($"A rover heading {Heading} collided with another rover at ({Position.X}, {Position.Y}).");

            // Move the rover.
            Position = pt;
        }

        /// <summary> Gets the position that the rover will have if it moves once with the current heading. </summary>
        /// <returns> The next position. </returns>
        private Point GetNextPosition()
        {
            int x = Position.X;
            int y = Position.Y;
            switch (Heading)
            {
                case Direction.North:
                    y++;
                    break;

                case Direction.South:
                    y--;
                    break;

                case Direction.East:
                    x++;
                    break;

                case Direction.West:
                    x--;
                    break;

                default:
                    throw new NotImplementedException($"Not implemented for the {Heading} heading.");
            }

            return new Point(x, y);
        }   

        /// <summary> Assigns the provided commands to the command queue, clearing out any existing commands. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="commands"/> is null. </exception>
        /// <exception cref="ArgumentException">     Thrown if <paramref name="commands"/> is empty. </exception>
        /// <param name="commands"> The commands to assign. </param>
        public void AssignCommands(IEnumerable<Command> commands)
        {
            if (commands == null)
                throw new ArgumentNullException(nameof(commands));
            if (!commands.Any())
                throw new ArgumentException("The provided instruction set is empty.", nameof(commands));

            _commandQueue.Clear();
            foreach (Command cmd in commands)
                _commandQueue.Enqueue(cmd);
        }

        /// <summary> Appends a command to the end of the command queue. </summary>
        /// <param name="command"> The command. </param>
        public void AppendCommand(Command command)
        {
            _commandQueue.Enqueue(command);
        }

        /// <summary> Clears the command queue. </summary>
        public void ClearCommands()
        {
            _commandQueue.Clear();
        }

        /// <summary> Executes the next command in the rover's instruction set. </summary>
        /// <exception cref="InvalidOperationException"> Thrown if there are no commands to execute. </exception>
        /// <exception cref="NotImplementedException">   Thrown if the command is not implemented. </exception>
        public void ExecuteNextCommand()
        {
            if (!HasCommands)
                throw new InvalidOperationException("There are no commands to execute.");

            while (_commandQueue.Count > 0)
            {
                Command cmd = _commandQueue.Dequeue();
                switch (cmd)
                {
                    case Command.TurnLeft:
                        TurnLeft();
                        break;

                    case Command.TurnRight:
                        TurnRight();
                        break;

                    case Command.Move:
                        Move();
                        break;

                    default:
                        throw new NotImplementedException($"Not implemented for the {cmd} command.");
                }
            }
        }

        /// <summary> Gets the string representation of the rover's position and heading. </summary>
        /// <returns> The string representation of the rover. </returns>
        public override string ToString()
        {
            return $"{Position.X} {Position.Y} {Heading.ToString()[0]}";
        }
    }
}
