﻿namespace MarsRovers
{
    /// <summary> An enum representing cardinal directions. </summary>
    /// <remarks> The enum values are assigned integer values to support turning via integer arithmetic. </remarks>
    public enum Direction
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }

    /// <summary> An enum representing valid rover commands. </summary>
    public enum Command
    {
        TurnLeft,
        TurnRight,
        Move
    }
}