﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MarsRovers
{
    /// <summary> The main class. Runs the Mars Rover simulation. </summary>
    class Program
    {
        /// <summary> The entry point for the application. </summary>
        /// <param name="args"> Command-line arguments; not used here. </param>
        static void Main(string[] args)
        {
            Console.WriteLine("~~~ Hello, Mars! ~~~");
            Console.WriteLine();

            // Prompt for the north-east coordinate of the plateau.
            Point pt = PromptForNorthEastCoordinate();
            Simulation simulation = new Simulation(pt.X, pt.Y);

            // Prompt the user to enter rovers and their instruction set.
            // Loops until the user chooses not to add more rovers.
            while (true)
            {
                Rover rover = PromptForRover(simulation);
                Queue<Command> instructions = PromptForInstructions();
                rover.AssignCommands(instructions);

                // Prompt the user whether to add more rovers.
                if (!PromptToAddMore())
                    break;
            }

            try
            {
                Console.WriteLine();
                Console.WriteLine("Output:");

                // Run the simulation.
                simulation.Run();

                // Print the final coordinates and headings.
                foreach (Rover rover in simulation.Rovers)
                    Console.WriteLine(rover);
            }
            catch (InvalidOperationException err)
            {
                // Typically happens if a rover fell off the plateau or collided with another rover.
                Console.Error.WriteLine(err.Message);
                Console.Error.WriteLine("Aborting simulation...");
            }            

            // Pause until the user presses a key, then exit.
            Console.WriteLine();
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey(true);
        }

        /// <summary> Prompts the user for the north-east coordinate of the plateau. </summary>
        /// <returns> The coordinates of the north-east corner of the plateau. </returns>
        private static Point PromptForNorthEastCoordinate()
        {
            // Loops until valid coordinates are entered.
            while (true)
            {
                // Prompt the user for the north-east corner of the plateau.
                Console.WriteLine("Enter the coordinates of the north-east corner of the plateau (e.g., 5 5).");
                String input = Console.ReadLine();
                Console.WriteLine();

                if (TextParser.IsCoordinateString(input))
                {
                    Point northEast = TextParser.ParseCoordinate(input);

                    // Check if the coordinates are valid.
                    if (northEast.X < 0 || northEast.Y < 0)
                        Console.WriteLine("The X and Y coordinates for the north-east corner must be non-negative.");
                    else
                        return northEast;
                }
                else
                    Console.WriteLine("Invalid format.");
            }
        }

        /// <summary> Prompts the user for the initial position and heading of a rover. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="simulation"/> is null. </exception>
        /// <param name="simulation"> The simulation. </param>
        /// <returns> The initialized rover. </returns>
        private static Rover PromptForRover(Simulation simulation)
        {
            if (simulation == null)
                throw new ArgumentNullException(nameof(simulation));

            // Loop until a valid rover string has been entered.
            while (true)
            {
                // Prompt the user for the rover's parameters.
                Console.WriteLine("Enter the coordinates and heading of the rover (e.g., 1 2 N).");
                Console.WriteLine("The heading must be one character from [NEWS].");
                String input = Console.ReadLine();
                Console.WriteLine();

                if (TextParser.IsRoverString(input))
                {
                    Point pt = TextParser.ParseRoverPosition(input);

                    // Check whether the rover is out of bounds of the plateau and whether the heading is valid..
                    bool badX = pt.X < 0 || pt.X > simulation.MaxX;
                    bool badY = pt.Y < 0 || pt.Y > simulation.MaxY;
                    bool badHeading = !TextParser.HasValidHeading(input);

                    // Warn the user if it is out of bounds or the heading is invalid.
                    if (badX)
                        Console.WriteLine($"The X coordinate must be between 0 and {simulation.MaxX}.");
                    if (badY)
                        Console.WriteLine($"The Y coordinate must be betwen 0 and {simulation.MaxY}.");
                    if (badHeading)
                        Console.WriteLine("The heading must be a character from [NEWS].");

                    // If everything is valid, add the rover to the simulation and return it.
                    if (!(badX || badY || badHeading))
                        return simulation.AddRover(pt.X, pt.Y, TextParser.ParseRoverHeading(input));
                }
                else
                    Console.WriteLine("Invalid format.");
            }
        }

        /// <summary> Prompts the user to enter the instruction set for a rover. </summary>
        /// <returns> The rover's instruction set. </returns>
        private static Queue<Command> PromptForInstructions()
        {
            // Loop until a valid command set has been entered.
            while (true)
            {
                // Prompt the user for the rover's instruction set.
                Console.WriteLine("Enter the rover's instruction set with no whitespace (e.g., LMRRM)");
                Console.WriteLine("Available instructions are L, R, and M");
                Console.WriteLine("(turn left, turn right, and move, respectively.");
                String input = Console.ReadLine();
                Console.WriteLine();

                // If the instruction set is valid, parse it and return the result.
                if (TextParser.IsCommandString(input))
                    return TextParser.ParseCommands(input);
                else
                    Console.WriteLine("Unrecognized commands or invalid format.");
            }
        }

        /// <summary> Prompts the user whether to add another rover. </summary>
        /// <returns> True if the user wants to add another rover; false otherwise. </returns>
        private static bool PromptToAddMore()
        {
            Console.WriteLine("Would you like to add another rover? (y/n)");
            
            // Loop until a valid key has been pressed.
            while (true)
            {
                char confirmation = Console.ReadKey(true).KeyChar;
                switch (confirmation)
                {
                    case 'y':
                    case 'Y':
                        return true;

                    case 'n':
                    case 'N':
                        return false;
                }
            }
        }
    }
}
