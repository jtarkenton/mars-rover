﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MarsRovers
{
    /// <summary> Handles parsing of text for the main program. </summary>
    public static class TextParser
    {
        #region Patterns

        /// <summary>
        /// A regular expression for parsing a coordinate string.
        /// It matches any string that consists of two integers separated by whitespace.
        /// </summary>
        private const String COORDINATE_PATTERN = @"^(?<x>-?\d+)\s+(?<y>-?\d+)$";

        /// <summary>
        /// A regular expression for parsing the rover initialization string.
        /// It matches any string that consists of two integers and a character corresponding to a cardinal direction,
        /// separated by whitespace.
        /// </summary>
        private const String ROVER_PATTERN = @"^(?<x>-?\d+)\s+(?<y>-?\d+)\s+(?<heading>\w+)$";

        /// <summary> A regular expression for validating the rover heading string. </summary>
        private const String HEADING_PATTERN = @"^[NEWS]$";

        /// <summary>
        /// A regular expression for validating the rover instruction string.
        /// It matches any string that consists only of the L, R, and M characters.
        /// </summary>
        private const string INSTRUCTION_PATTERN = @"^[LRM]+$";

        #endregion

        #region Pattern Testing

        /// <summary> Determines whether the provided input string matches the coordinate pattern. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> True if the input string matches the coordinate pattern; false otherwise. </returns>
        public static bool IsCoordinateString(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            return Regex.IsMatch(input, COORDINATE_PATTERN);
        }

        /// <summary> Determines whether the provided input string matches the rover pattern. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> True if the input string matches the rover pattern; false otherwise. </returns>
        public static bool IsRoverString(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            return Regex.IsMatch(input, ROVER_PATTERN, RegexOptions.IgnoreCase);
        }

        /// <summary> Determines whether the provided input string matches the heading pattern. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> True if the input string matches the heading pattern; false otherwise. </returns>
        public static bool HasValidHeading(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            // Parse the rover string.
            var match = Regex.Match(input, ROVER_PATTERN, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                // Validate the heading.
                String heading = match.Groups["heading"].Value;
                return Regex.IsMatch(heading, HEADING_PATTERN, RegexOptions.IgnoreCase);
            }

            return false;
        }

        /// <summary> Determines whether the provided input string matches the command pattern. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> True if the input string matches the command pattern; false otherwise. </returns>
        public static bool IsCommandString(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            return Regex.IsMatch(input, INSTRUCTION_PATTERN, RegexOptions.IgnoreCase);
        }

        #endregion

        #region Parsing

        /// <summary> Parses a coordinate from the provided input string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> The parsed coordinate. </returns>
        public static Point ParseCoordinate(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var match = Regex.Match(input, COORDINATE_PATTERN);
            if (!match.Success)
                throw new ArgumentException("Invalid input.", nameof(input));

            int x = Int32.Parse(match.Groups["x"].Value);
            int y = Int32.Parse(match.Groups["y"].Value);

            return new Point(x, y);
        }

        /// <summary> Parses the rover's position from the provided rover input string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The rover input string. </param>
        /// <returns> The parsed position. </returns>
        public static Point ParseRoverPosition(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            // Parse the rover string.
            var match = Regex.Match(input, ROVER_PATTERN);
            if (!match.Success)
                throw new ArgumentException("Invalid format.", nameof(input));

            // Parse the coordinates and return them.
            int x = Int32.Parse(match.Groups["x"].Value);
            int y = Int32.Parse(match.Groups["y"].Value);
            return new Point(x, y);
        }

        /// <summary> Parses the rover's heading from the provided rover input string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <param name="input"> The rover input string. </param>
        /// <returns> The parsed heading. </returns>
        public static Direction ParseRoverHeading(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            // Parse the rover string.
            var match = Regex.Match(input, ROVER_PATTERN);
            if (!match.Success)
                throw new ArgumentException("Invalid format.", nameof(input));

            // Parse the heading.
            return ParseHeading(match.Groups["heading"].Value);
        }

        /// <summary> Parses the heading from the provided input string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <exception cref="ArgumentException">     Thrown if <paramref name="input"/> is not a valid heading. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> The parsed heading. </returns>
        public static Direction ParseHeading(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            switch (input.ToUpperInvariant())
            {
                case "N":
                    return Direction.North;

                case "E":
                    return Direction.East;

                case "W":
                    return Direction.West;

                case "S":
                    return Direction.South;

                default:
                    throw new ArgumentException("The heading is not valid.", nameof(input));
            }
        }

        /// <summary> Parses the command queue from the provided input string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown if <paramref name="input"/> is null. </exception>
        /// <exception cref="ArgumentException">     Thrown if <paramref name="input"/> contains invalid characters. </exception>
        /// <param name="input"> The input string. </param>
        /// <returns> The parsed command queue. </returns>
        public static Queue<Command> ParseCommands(String input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            if (String.IsNullOrWhiteSpace(input))
                throw new ArgumentException(nameof(input));

            // Parse each command and add them to the queue.
            Queue<Command> queue = new Queue<Command>();
            foreach (char c in input)
                queue.Enqueue(ParseCommand(c));

            return queue;
        }

        /// <summary> Parses a command from the provided character. </summary>
        /// <exception cref="ArgumentException"> Thrown if <paramref name="cmd"/> does not correspond to a valid command. </exception>
        /// <param name="cmd"> The command character. </param>
        /// <returns> The parsed command. </returns>
        private static Command ParseCommand(char cmd)
        {
            switch (cmd)
            {
                case 'L':
                case 'l':
                    return Command.TurnLeft;

                case 'R':
                case 'r':
                    return Command.TurnRight;

                case 'M':
                case 'm':
                    return Command.Move;

                default:
                    throw new ArgumentException($"The command \"{cmd}\" is not valid.", nameof(cmd));
            }
        }

        #endregion
    }
}
