﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MarsRovers
{
    /// <summary> Executes the simulation and manages the plateau and rovers. </summary>
    public class Simulation
    {
        #region Member Variables and Properties

        private IList<Rover> _rovers;

        /// <summary> Gets or sets the maximum valid X coordinate for rovers on the plateau. </summary>
        public int MaxX { get; private set; }

        /// <summary> Gets or sets the maximum valid Y coordinate for rovers on the plateau. </summary>
        public int MaxY { get; private set; }

        /// <summary> Gets an ordered collection of the rovers on the plateau. </summary>
        /// <remarks> We don't expose the list directly as there is no need to allow anything else to modify it. </remarks>
        public IEnumerable<Rover> Rovers
        {
            get
            {
                foreach (Rover rover in _rovers)
                    yield return rover;
            }
        }

        #endregion

        /// <summary> Constructs a new simulation object with the specified north-east coordinates. </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown if either coordinate is negative. </exception>
        /// <param name="northEastX"> The X coordinate of the north-east corner of the plateau. </param>
        /// <param name="northEastY"> The Y coordinate of the north-east corner of the plateau. </param>
        public Simulation(int northEastX, int northEastY)
        {
            if (northEastX < 0)
                throw new ArgumentOutOfRangeException(nameof(northEastX), "The X coordinate must be non-negative.");
            if (northEastY < 0)
                throw new ArgumentOutOfRangeException(nameof(northEastY), "The Y coordinate must be non-negative.");

            MaxX = northEastX;
            MaxY = northEastY;
            _rovers = new List<Rover>();
        }

        /// <summary> Adds a new rover to the simulation. </summary>
        /// <param name="x">       The X coordinate of the rover's initial location. </param>
        /// <param name="y">       The Y coordinate of the rover's initial location.  </param>
        /// <param name="heading"> The rover's initial heading. </param>
        /// <returns> The added rover. </returns>
        public Rover AddRover(int x, int y, Direction heading)
        {
            // Check if the rover's position is out of bounds.
            if (x < 0 || x > MaxX)
                throw new ArgumentOutOfRangeException(nameof(x));
            if (y < 0 || y > MaxY)
                throw new ArgumentOutOfRangeException(nameof(y));

            // Check if there's already a rover at the specified position.
            Point pt = new Point(x, y);
            if (_rovers.Any(otherRover => pt.Equals(otherRover.Position)))
                throw new ArgumentException($"There is already a rover at ({x}, {y}).");

            // Add and return an initialized rover.
            Rover rover = new Rover(x, y, heading);
            rover.Simulation = this;
            _rovers.Add(rover);
            return rover;
        }

        /// <summary> Runs the simulation, moving rovers according to their instruction sets. </summary>
        public void Run()
        {
            for (int i = 0; i < _rovers.Count; i++)
            {
                Rover rover = _rovers[i];
                while (rover.HasCommands)
                    rover.ExecuteNextCommand();
            }
        }
    }
}
