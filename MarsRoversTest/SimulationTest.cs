﻿using MarsRovers;
using System;
using System.Collections.Generic;
using System.Drawing;
using Xunit;

namespace MarsRoversTest
{
    /// <summary> Unit tests for the Simulation class. </summary>
    public class SimulationTest
    {
        /// <summary> Tests a variety of valid parameters for the constructor to ensure that properties are assigned properly. </summary>
        /// <param name="northEastX"> The X coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="northEastY"> The Y coordinate of the north-eastern corner of the plateau. </param>
        [Theory]
        [InlineData(1, 1)]
        [InlineData(32, 64)]
        [InlineData(0, 0)]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        public void TestValidConstructorParams(int northEastX, int northEastY)
        {
            Simulation simulation = new Simulation(northEastX, northEastY);
            Assert.Equal(northEastX, simulation.MaxX);
            Assert.Equal(northEastY, simulation.MaxY);
        }

        /// <summary> Tests a variety of invalid parameters for the constructor to ensure that appropriate exceptions are thrown. </summary>
        /// <param name="northEastX"> The X coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="northEastY"> The Y coordinate of the north-eastern corner of the plateau. </param>
        [Theory]
        [InlineData(-1, 10)]
        [InlineData(-1, 0)]
        [InlineData(0, -1)]
        [InlineData(10, -1)]
        public void TestInvalidConstructorParams(int northEastX, int northEastY)
        {
            Assert.Throws<ArgumentOutOfRangeException>(new Action(() => new Simulation(northEastX, northEastY)));
        }

        /// <summary> Tests the simulation of the exploration mission. </summary>
        /// <param name="x">      The X coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="y">      The Y coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="rovers"> An array of arrays representing the input and expected output for each rover.
        ///                       Each element of the outer array contains the input and expected output for one rover:
        ///                       * The first element of the inner array is the user input for the rover's initial coordinates and heading.
        ///                       * The second element of the inner array is the user input for the rover's instruction set.
        ///                       * The third element of the inner array is the expected output after simulation. </param>
        [Theory]
        [InlineData(5, 5, new[] { "1 2 N", "LMLMLMLMM", "1 3 N" }, new[] { "3 3 E", "MMRMMRMRRM", "5 1 E" })]
        public void TestSimulation(int x, int y, params String[][] rovers)
        {
            Simulation simulation = new Simulation(x, y);
            IDictionary<Rover, String> outputDict = new Dictionary<Rover, String>();
            for (int i = 0; i < rovers.Length; i++)
            {
                // Get the arguments for the rover.
                String init = rovers[i][0];
                String instructions = rovers[i][1];
                String expectedOutput = rovers[i][2];
                
                // Construct the rover.
                Point pt = TextParser.ParseRoverPosition(init);
                Direction heading = TextParser.ParseRoverHeading(init);
                Rover rover = simulation.AddRover(pt.X, pt.Y, heading);
                rover.AssignCommands(TextParser.ParseCommands(instructions));

                // Take note of the rover's expected output.
                outputDict[rover] = rovers[i][2];
            }

            // Run the simulation and validate each rover's output against the expected output.
            simulation.Run();
            foreach (Rover rover in simulation.Rovers)
                Assert.Equal(outputDict[rover], rover.ToString());
        }

        /// <summary> Tests the simulation for collision and fall detection. </summary>
        /// <param name="x">       The X coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="y">       The Y coordinate of the north-eastern corner of the plateau. </param>
        /// <param name="rovers">  An array of arrays representing the input for each rover.
        ///                        Each element of the outer array contains the input for one rover:
        ///                        * The first element of the inner array is the user input for the rover's initial coordinates and heading.
        ///                        * The second element of the inner array is the user input for the rover's instruction set. </param>
        [Theory]
        [InlineData(0, 0, new[] { "0 0 N", "M"})] // Test falling off the plateau
        [InlineData(1, 1, new[] { "0 0 N", "MRM"}, new[] { "1 1 N", "LL" })] // Test collision
        public void TestFailedSimulation(int x, int y, params String[][] rovers)
        {
            Simulation simulation = new Simulation(x, y);
            for (int i = 0; i < rovers.Length; i++)
            {
                // Get the arguments for the rover.
                String init = rovers[i][0];
                String instructions = rovers[i][1];

                // Construct the rover.
                Point pt = TextParser.ParseRoverPosition(init);
                Direction heading = TextParser.ParseRoverHeading(init);
                Rover rover = simulation.AddRover(pt.X, pt.Y, heading);
                rover.AssignCommands(TextParser.ParseCommands(instructions));
            }

            // Ensure that the simulation fails.
            Assert.Throws<InvalidOperationException>(new Action(() => simulation.Run()));
        }

        /// <summary> Tests the simulation to ensure that the rover order affects whether a collision can occur. </summary>
        [Fact]
        public void TestRoverOrder()
        {
            // Test a successfully evaded collision (first rover moves out of the way of the second rover).
            Simulation simulation = new Simulation(10, 10);
            Rover first = simulation.AddRover(1, 1, Direction.South);
            first.AssignCommands(TextParser.ParseCommands("M"));
            Rover second = simulation.AddRover(0, 0, Direction.North);
            second.AssignCommands(TextParser.ParseCommands("MRMM"));

            // Run the simulation and validate the output.
            simulation.Run();
            Assert.Equal(new Point(1, 0), first.Position);
            Assert.Equal(new Point(2, 1), second.Position);

            // Test the same rover setup but in reverse order. A collision should occur.
            simulation = new Simulation(10, 10);
            first = simulation.AddRover(0, 0, Direction.North);
            first.AssignCommands(TextParser.ParseCommands("MRMM"));
            second = simulation.AddRover(1, 1, Direction.South);
            second.AssignCommands(TextParser.ParseCommands("M"));

            // Run the simulation and check for a thrown exception.
            Assert.Throws<InvalidOperationException>(new Action(() => simulation.Run()));
        }
    }
}
