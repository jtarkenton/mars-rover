using MarsRovers;
using System;
using System.Drawing;
using Xunit;

namespace MarsRoversTest
{
    /// <summary> Unit tests for the Rover class. </summary>
    public class RoverTest
    {
        /// <summary> Tests rover construction to make sure it assigns properties appropriately. </summary>
        /// <param name="x">       The X coordinate of the rover. </param>
        /// <param name="y">       The Y coordinate of the rover. </param>
        /// <param name="heading"> The rover's heading. </param>
        [Theory]
        [InlineData(0, 0, Direction.North)]
        [InlineData(13, 37, Direction.East)]
        public void TestConstructor(int x, int y, Direction heading)
        {
            Rover rover = new Rover(x, y, heading);
            Assert.Equal(new Point(x, y), rover.Position);
            Assert.Equal(heading, rover.Heading);
        }

        /// <summary> Tests rover turning. </summary>
        [Fact]
        public void TestTurning()
        {
            Rover rover = new Rover(0, 0, Direction.North);

            // Turn left until we reach the original position.
            rover.TurnLeft();
            Assert.Equal(Direction.West, rover.Heading);

            rover.TurnLeft();
            Assert.Equal(Direction.South, rover.Heading);

            rover.TurnLeft();
            Assert.Equal(Direction.East, rover.Heading);

            rover.TurnLeft();
            Assert.Equal(Direction.North, rover.Heading);

            // Turn right until we reach the original position.
            rover.TurnRight();
            Assert.Equal(Direction.East, rover.Heading);

            rover.TurnRight();
            Assert.Equal(Direction.South, rover.Heading);

            rover.TurnRight();
            Assert.Equal(Direction.West, rover.Heading);

            rover.TurnRight();
            Assert.Equal(Direction.North, rover.Heading);
        }

        /// <summary> Tests rover movement. </summary>
        [Fact]
        public void TestMoving()
        {
            Simulation simulation = new Simulation(2, 2);
            Rover rover = simulation.AddRover(1, 1, Direction.North);

            // Head north from center
            rover.Move();
            Assert.Equal(new Point(1, 2), rover.Position);

            // Turn right and head east
            rover.TurnRight();
            rover.Move();
            Assert.Equal(new Point(2, 2), rover.Position);

            // Turn right and head south
            rover.TurnRight();
            rover.Move();
            Assert.Equal(new Point(2, 1), rover.Position);

            // Head south again
            rover.Move();
            Assert.Equal(new Point(2, 0), rover.Position);

            // Turn right and head west
            rover.TurnRight();
            rover.Move();
            Assert.Equal(new Point(1, 0), rover.Position);

            // Turn right and head north to our original position
            rover.TurnRight();
            rover.Move();
            Assert.Equal(new Point(1, 1), rover.Position);
        }

        /// <summary> Tests whether the Move method detects a fall off the plateau. </summary>
        [Fact]
        public void TestFall()
        {
            // Test that moving in any direction on a 1x1 plateau causes a fall.
            foreach (Direction heading in Enum.GetValues(typeof(Direction)))
            {
                Simulation simulation = new Simulation(0, 0);
                Rover rover = simulation.AddRover(0, 0, heading);
                Assert.Throws<InvalidOperationException>(new Action(() => rover.Move()));
            }
        }

        /// <summary> Tests whether the Move method detects collisions with other rovers. </summary>
        [Fact]
        public void TestCollision()
        {
            Simulation simulation = new Simulation(2, 2);

            // Add rovers to the 3x3 plateau in a + shape, with outer rovers facing the center.
            Rover center = simulation.AddRover(1, 1, Direction.North);
            Rover north = simulation.AddRover(1, 2, Direction.South);
            Rover south = simulation.AddRover(1, 0, Direction.North);
            Rover east = simulation.AddRover(2, 1, Direction.West);
            Rover west = simulation.AddRover(0, 1, Direction.East);

            // Test that moving rovers into the center square causes a collision with the rover in the center.
            Assert.Throws<InvalidOperationException>(new Action(() => north.Move()));
            Assert.Throws<InvalidOperationException>(new Action(() => south.Move()));
            Assert.Throws<InvalidOperationException>(new Action(() => east.Move()));
            Assert.Throws<InvalidOperationException>(new Action(() => west.Move()));
        }
    }
}
