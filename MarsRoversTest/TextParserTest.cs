﻿using MarsRovers;
using System;
using System.Collections.Generic;
using System.Drawing;
using Xunit;

namespace MarsRoversTest
{
    /// <summary> Unit tests for the TextParser class. </summary>
    public class TextParserTest
    {
        /// <summary> Tests that all valid headings can be parsed correctly. </summary>
        /// <param name="input">   The input string. </param>
        /// <param name="heading"> The expected parsed heading. </param>
        [Theory]
        [InlineData("n", Direction.North)]
        [InlineData("N", Direction.North)]
        [InlineData("e", Direction.East)]
        [InlineData("E", Direction.East)]
        [InlineData("w", Direction.West)]
        [InlineData("W", Direction.West)]
        [InlineData("s", Direction.South)]
        [InlineData("S", Direction.South)]
        public void TestValidHeadings(String input, Direction heading)
        {
            Assert.Equal(heading, TextParser.ParseHeading(input));
        }

        /// <summary> Tests a variety of invalid headings to ensure that appropriate exceptions are thrown. </summary>
        /// <param name="input"> The input string. </param>
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("North")]
        [InlineData("NW")]
        [InlineData("x")]
        [InlineData(" N ")]
        public void TestInvalidHeadings(String input)
        {
            if (input == null)
                Assert.Throws<ArgumentNullException>(new Action(() => TextParser.ParseHeading(input)));
            else
                Assert.Throws<ArgumentException>(new Action(() => TextParser.ParseHeading(input)));
        }

        /// <summary> Tests several valid command sets to ensure that they are parsed correctly. </summary>
        /// <param name="input">    The input string. </param>
        /// <param name="commands"> The expected parsed commands. </param>
        [Theory]
        [InlineData("LRM", Command.TurnLeft, Command.TurnRight, Command.Move)]
        [InlineData("lrmMRL", Command.TurnLeft, Command.TurnRight, Command.Move, Command.Move, Command.TurnRight, Command.TurnLeft)]
        [InlineData("MMMRRL", Command.Move, Command.Move, Command.Move, Command.TurnRight, Command.TurnRight, Command.TurnLeft)]
        public void TestValidCommandQueue(String input, params Command[] commands)
        {
            Queue<Command> expected = new Queue<Command>(commands);
            Queue<Command> actual = TextParser.ParseCommands(input);

            Assert.Equal(expected.Count, actual.Count);

            while (expected.Count > 0)
                Assert.Equal(expected.Dequeue(), actual.Dequeue());
        }

        /// <summary> Tests several invalid command sets to ensure that appropriate exceptions are thrown. </summary>
        /// <param name="input"> The input string. </param>
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("UUDDLRLR")]
        [InlineData("LLRR/MM")]
        [InlineData(" LRM ")]
        [InlineData("L R M")]
        public void TestInvalidCommandQueue(String input)
        {
            if (input == null)
                Assert.Throws<ArgumentNullException>(new Action(() => TextParser.ParseCommands(input)));
            else
                Assert.Throws<ArgumentException>(new Action(() => TextParser.ParseCommands(input)));
        }

        /// <summary> Tests the parsing of headings from several valid rover input strings. </summary>
        /// <param name="input">   The input string. </param>
        /// <param name="heading"> The expected parsed heading. </param>
        [Theory]
        [InlineData("0 0 N", Direction.North)]
        [InlineData("0 0 n", Direction.North)]
        [InlineData("0 0 E", Direction.East)]
        [InlineData("0 0 e", Direction.East)]
        [InlineData("0 0 W", Direction.West)]
        [InlineData("0 0 w", Direction.West)]
        [InlineData("0 0 S", Direction.South)]
        [InlineData("0 0 s", Direction.South)]
        public void TestRoverHeading(String input, Direction heading)
        {
            Assert.Equal(heading, TextParser.ParseRoverHeading(input));
        }

        /// <summary> Tests the parsing of positions from several valid rover input strings. </summary>
        /// <param name="input"> The input string. </param>
        /// <param name="x">     The expected parsed X coordinate. </param>
        /// <param name="y">     The expected parsed Y coordinate. </param>
        [Theory]
        [InlineData("0 0 N", 0, 0)]
        [InlineData("-4 5 E", -4, 5)]
        [InlineData("5 -5 W", 5, -5)]
        [InlineData("13 37 S", 13, 37)]
        public void TestRoverPosition(String input, int x, int y)
        {
            Assert.Equal(new Point(x, y), TextParser.ParseRoverPosition(input));
        }

        /// <summary> Tests the parsing of coordinates from several valid plateau input strings. </summary>
        /// <param name="input"> The input string. </param>
        /// <param name="x">     The expected parsed X coordinate. </param>
        /// <param name="y">     The expected parsed Y coordinate. </param>
        [Theory]
        [InlineData("0 0", 0, 0)]
        [InlineData("-4 5", -4, 5)]
        [InlineData("5 -5", 5, -5)]
        [InlineData("13 37", 13, 37)]
        public void TestCoordinate(String input, int x, int y)
        {
            Assert.Equal(new Point(x, y), TextParser.ParseCoordinate(input));
        }
    }
}
